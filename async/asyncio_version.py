from multiprocessing import Process
import asyncio
import aiohttp
from aiohttp import web
from HTTPHandlers import OK_handler, B_Handler
import time


def a_tasks(x, y):
    async def http_listen(OK_app):
        runner = web.AppRunner(OK_app)
        await runner.setup()
        site = web.TCPSite(runner, 'localhost', 1234)
        await site.start()

    async def http_requests(task_id):
        number = 1
        async with aiohttp.ClientSession() as session:
            for i in range(y):
                async with session.get('http://localhost:1235',
                                       params={'task_id': task_id, 'number': number}) as result:
                    number = await result.text()

    loop = asyncio.get_event_loop()
    tasks = list()
    for task_id in range(1, x+1):
        tasks.append(loop.create_task(http_requests(task_id)))

    OK_app = web.Application()
    OK_app.add_routes([web.get('/', OK_handler)])
    server_task = loop.create_task(http_listen(OK_app))
    for task in tasks:
        loop.run_until_complete(task)

    loop.run_until_complete(OK_app.shutdown())


def b_tasks():
    B_app = web.Application()
    B_app.add_routes([web.get('/', B_Handler)])
    web.run_app(B_app, host='localhost', port=1235)


if __name__ == '__main__':
    start_time = time.time()
    process_A = Process(target=a_tasks, args=(25, 25))
    process_B = Process(target=b_tasks)
    process_B.start()
    while not process_B.is_alive():
        pass
    process_A.start()
    process_A.join()
    process_B.terminate()
    process_B.join()
    print("%s seconds" % (time.time() - start_time))

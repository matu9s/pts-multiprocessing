from aiohttp import web
import aiohttp


async def OK_handler(request):
    return web.Response(text="OK")


async def B_Handler(request):
    async with aiohttp.ClientSession() as session:
        async with session.get('http://localhost:1234') as result:
            result_text = await result.text()
            if result_text == "OK":
                print(request.query['task_id'], request.query['number'])
                return web.Response(text=str(int(request.query['number'])+1))

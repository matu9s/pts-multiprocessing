import http.server
from http import server
from urllib.parse import parse_qs, urlparse
import requests
from threading import Thread
from socketserver import ThreadingMixIn


class OKHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write("OK".encode())


class BHandler(ThreadingMixIn, server.BaseHTTPRequestHandler):
    def do_GET(self):
        def http_respond(number):
            result = requests.get('http://localhost:1234')
            if result.text == "OK":
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(number.encode())
        query_components = parse_qs(urlparse(self.path).query)
        responding_task = Thread(target=http_respond, args=(str(int(query_components['number'][0]) + 1),))
        responding_task.start()
        responding_task.join()


def run_server(host, port, handler):
    server_address = (host, port)
    httpd = http.server.HTTPServer(server_address, handler)
    httpd.serve_forever()
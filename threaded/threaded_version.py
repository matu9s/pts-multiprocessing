from multiprocessing import Process
from threading import Thread
import requests
from HTTPHandlers import run_server, OKHandler, BHandler
import sys
import time


def a_tasks(x, y):
    def http_listen():
        run_server('localhost', 1234, OKHandler)

    def http_requests(task_id):
        number = 1
        for i in range(y):
            result = requests.get('http://localhost:1235', params={'task_id': task_id, 'number': number})
            number = result.text

    listening_task = Thread(target=http_listen)
    listening_task.start()
    thread_tasks = list()
    for task_id in range(1, x + 1):
        thread_task = Thread(target=http_requests, args=(task_id,))
        thread_task.start()
        thread_tasks.append(thread_task)
    for thread_task in thread_tasks:
        thread_task.join()
    sys.exit()


def b_tasks():
    run_server('localhost', 1235, BHandler)


if __name__ == '__main__':
    start_time = time.time()
    process_A = Process(target=a_tasks, args=(25, 25))
    process_B = Process(target=b_tasks)
    process_B.start()
    while not process_B.is_alive():
        pass
    process_A.start()
    process_A.join()
    process_B.terminate()
    process_B.join()
    print("%s seconds" % (time.time() - start_time))
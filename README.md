# Comparison

Async version seems to be much faster than threaded version.
Programs were tested and finished with the following results:
|x   |y   |Async time  |Threaded time|
|----|----|------------|-------------|
|5   |5   |0.07580 s   |0.09829 s    |
|10  |1   |0.04695 s   |1.10316 s    |
|1   |10  |0.04531 s   |0.05443 s    |
|10  |10  |0.20857 s   |1.15861 s    |
|30  |30  |1.37578 s   |4.51392 s    |
|50  |50  |3.51881 s   |14.63413 s   |
--------------------------------------
In each of the tests async version is faster. In case x = 50, y = 50 it is approximately 4 times faster
than threaded solution, which is quite a noticeable difference (approximately 11 seconds).
